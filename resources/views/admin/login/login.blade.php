<!DOCTYPE html>
<head>
<meta charset="utf-8">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="{{asset('admin/')}}/css/stylelogin.css" media="screen" />
</head>
<body>
<div class="container">
	<section id="content">
		{!!Form::open(['url' => '/login', 'method' => 'POST'])!!}
			 {{ csrf_field() }}
			<h1>Admin Login</h1>
			<div class="form-group">
				{{Form::text('email', null, ['class'=>'form-control','placeholder'=>'Email'])}}
				  @if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				{{-- <input type="text" placeholder="Email" required="" name="email"/> --}}
			</div>
			<div class="form-group">
				{{Form::password('password', ['class'=>'form-control','placeholder'=> 'Password'])}}
				  @if ($errors->has('password'))
					<span class="help-block">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
				{{-- <input type="password" placeholder="Password" required="" name="password"/> --}}
			</div>
			<div class="form-group">
				{{Form::submit('Log In', ['class'=>'btn btn-info','name'=> 'btn'])}}
			</div>
			{{-- <div class="checkbox" >
				<label>{Form::checkbox('name', 'Remember Me');} Remember Me</label>
			</div> --}}
		{!!Form::close()!!}
		<div class="button">
			<a href="#">Training with live project</a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
</html>