       <div class="grid_12 header-repeat">
            <div id="branding">
                <div class="floatleft logo">
                    <img src="{{asset('public/admin/')}}/img/livelogo.png" alt="Logo" />
				</div>
				<div class="floatleft middle">
					<h1>Training with live project</h1>
					<p>www.trainingwithliveproject.com</p>
				</div>
                <div class="floatright">
                    <div class="floatleft">
                        <img src="{{asset('public/admin/')}}/img/img-profile.jpg" alt="Profile Pic" /></div>
                    <div class="floatleft marginleft10">
                        <ul class="inline-ul floatleft">
                            <li>Hello {{ Auth::user()->name }}</li>
                            <li><a href= href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div class="grid_12">
              <ul class="nav main">
                  <li class="ic-dashboard"><a href="{{url('/dashboard')}}"><span>Dashboard</span></a> </li>
                  <li class="ic-form-style"><a href=""><span>User Profile</span></a></li>
          <li class="ic-typography"><a href="changepassword.html"><span>Change Password</span></a></li>
          <li class="ic-grid-tables"><a href="inbox.html"><span>Inbox</span></a></li>
                  <li class="ic-charts"><a href="postlist.html"><span>Visit Website</span></a></li>
              </ul>
        </div>
        <div class="clear">
        </div>