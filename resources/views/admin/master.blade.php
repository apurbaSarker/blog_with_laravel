<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title> Admin</title>
<link rel="stylesheet" type="text/css" href="{{asset('public/admin/')}}/css/reset.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/')}}/css/text.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/')}}/css/grid.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/')}}/css/layout.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{asset('public/admin/')}}/css/nav.css" media="screen" />
    <link href="{{asset('public/admin/')}}/css/table/demo_page.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN: load jquery -->
    <script src="{{asset('public/admin/')}}/js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('public/admin/')}}/js/jquery-ui/jquery.ui.core.min.js"></script>
    <script src="{{asset('public/admin/')}}/js/jquery-ui/jquery.ui.widget.min.js" type="text/javascript"></script>
    <script src="{{asset('public/admin/')}}/js/jquery-ui/jquery.ui.accordion.min.js" type="text/javascript"></script>
    <script src="{{asset('public/admin/')}}/js/jquery-ui/jquery.effects.core.min.js" type="text/javascript"></script>
    <script src="{{asset('public/admin/')}}/js/jquery-ui/jquery.effects.slide.min.js" type="text/javascript"></script>
    <script src="{{asset('public/admin/')}}/js/jquery-ui/jquery.ui.mouse.min.js" type="text/javascript"></script>
    <script src="{{asset('public/admin/')}}/js/jquery-ui/jquery.ui.sortable.min.js" type="text/javascript"></script>
    <script src="{{asset('public/admin/')}}/js/table/jquery.dataTables.min.js" type="text/javascript"></script>
    <!-- END: load jquery -->
    <script type="text/javascript" src="js/table/table.js"></script>
    <script src="{{asset('public/admin/')}}/js/setup.js" type="text/javascript"></script>
	 <script type="text/javascript">
        $(document).ready(function () {
            setupLeftMenu();
		    setSidebarHeight();
        });
    </script>

</head>
<body>
    <div class="container_12">
 =====================================
@include('admin.includes.header')
@include('admin.includes.sidebar')
        <div class="grid_10">
		
           @yield('content')
        </div>
        <div class="clear">
        </div>
    </div>
    <div class="clear">
    </div>
    <div id="site_info">
        <p>
         &copy; Copyright <a href="http://trainingwithliveproject.com">Training with live project</a>. All Rights Reserved.
        </p>
    </div>
</body>
</html>
