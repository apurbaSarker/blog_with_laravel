<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
   public function index(){
    return view ('frontEnd.home.homeContent') ;

    }
   public function about(){
    return view ('frontEnd.about.aboutContent') ;

    }
   public function contact(){
    return view ('frontEnd.contact.contactContent') ;

    }
}